<?php
/**
 * @file fragment.install
 * Contains the install hooks for fragment module.
 */

/**
 * Implements hook_schema().
 */
function fragment_schema() {
  $schema['fragment'] = array(
    'description' => 'The base table for fragments.',
    'fields' => array(
      'fid' => array(
        'description' => 'The primary identifier for a fragment.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'title' => array(
        'description' => 'The title of this fragment, always treated as non-markup plain text.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'link' => array(
        'description' => 'The path or url this fragment links to.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ),
      'uid' => array(
        'description' => 'The {users}.uid that owns this fragment; initially, this is the user that created it.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'status' => array(
        'description' => 'Boolean indicating whether the fragment is published (visible to non-administrators).',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the fragment was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the fragment was most recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'fragment_changed'        => array('changed'),
      'fragment_created'        => array('created'),
    ),
    'unique keys' => array(
      'fid' => array('fid'),
    ),
    'foreign keys' => array(
      'fragment_author' => array(
        'table' => 'users',
        'columns' => array('uid' => 'uid')
      ),
    ),
    'primary key' => array('fid'),
  );
  return $schema;
}
